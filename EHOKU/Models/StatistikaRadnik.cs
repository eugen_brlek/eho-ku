﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EHOKU.Models
{
    public class dashboard
    {
        public int broj1 { get; set; }
        public int broj2 { get; set; }
        public int broj3 { get; set; }
        public int broj4 { get; set; }

        public IEnumerable<StatistikaRadnik> lista;


    }
    public class StatistikaRadnik
    {
        public decimal Idradnik { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Opis { get; set; }
        public decimal ZavrsenihPoslova { get; set; }
        public decimal Zaradeno { get; set; }
        public decimal Ocjena { get; set; }

    }
}
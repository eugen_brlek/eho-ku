﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Web;

namespace EHOKU.Models
{
    class Weather

    {
        public Object getWeatherForcast()
        {
            string appid = "03519e6233aa558e135e0121d3c0f644";
            string url = "http://api.openweathermap.org/data/2.5/weather?q=Rijeka&APPID=" + appid + "&units=metric";
            //synchronous client.
            var client = new WebClient();
            var content = client.DownloadString(url);
            var serializer = new JavaScriptSerializer();
            var jsonContent = serializer.Deserialize<Object>(content);
            return jsonContent;
        }

    }

}
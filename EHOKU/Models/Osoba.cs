﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EHOKU.Models
{
    public class Osoba
    {
        public string ID { get; set; }
        public string IME { get; set; }
        public string ROLA { get; set; }

        public static Osoba Currentuser
        {
            get
            {
                if (HttpContext.Current.Session["LogedUser"] != null)
                {
                    return (Osoba)HttpContext.Current.Session["LogedUser"];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                HttpContext.Current.Session["currentuser"] = value;
            }
        }


    }
}
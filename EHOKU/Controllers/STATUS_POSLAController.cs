﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace EHOKU.Controllers
{
    public class STATUS_POSLAController : Controller
    {
        private EHOKUEntities db = new EHOKUEntities();

        // GET: STATUS_POSLA
        public ActionResult Index()
        {
            return View(db.STATUS_POSLA.ToList());
        }

        // GET: STATUS_POSLA/Details/5
        public ActionResult Details(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            STATUS_POSLA sTATUS_POSLA = db.STATUS_POSLA.Find(id);
            if (sTATUS_POSLA == null)
            {
                return HttpNotFound();
            }
            return View(sTATUS_POSLA);
        }

        // GET: STATUS_POSLA/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: STATUS_POSLA/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_STATUS_POSLA,OPIS,NAPOMENA")] STATUS_POSLA sTATUS_POSLA)
        {
            if (ModelState.IsValid)
            {
                db.STATUS_POSLA.Add(sTATUS_POSLA);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sTATUS_POSLA);
        }

        // GET: STATUS_POSLA/Edit/5
        public ActionResult Edit(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            STATUS_POSLA sTATUS_POSLA = db.STATUS_POSLA.Find(id);
            if (sTATUS_POSLA == null)
            {
                return HttpNotFound();
            }
            return View(sTATUS_POSLA);
        }

        // POST: STATUS_POSLA/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_STATUS_POSLA,OPIS,NAPOMENA")] STATUS_POSLA sTATUS_POSLA)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sTATUS_POSLA).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sTATUS_POSLA);
        }

        // GET: STATUS_POSLA/Delete/5
        public ActionResult Delete(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            STATUS_POSLA sTATUS_POSLA = db.STATUS_POSLA.Find(id);
            if (sTATUS_POSLA == null)
            {
                return HttpNotFound();
            }
            return View(sTATUS_POSLA);
        }

        // POST: STATUS_POSLA/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(decimal id)
        {
            STATUS_POSLA sTATUS_POSLA = db.STATUS_POSLA.Find(id);
            db.STATUS_POSLA.Remove(sTATUS_POSLA);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

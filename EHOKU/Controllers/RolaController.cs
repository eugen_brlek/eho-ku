﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace EHOKU.Controllers
{
    public class RolaController : Controller
    {
        private EHOKUEntities db = new EHOKUEntities();

        // GET: Rola
        public ActionResult Index()
        {
            return View(db.ROLA.ToList());
        }

        // GET: Rola/Details/5
        public ActionResult Details(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ROLA rOLA = db.ROLA.Find(id);
            if (rOLA == null)
            {
                return HttpNotFound();
            }
            return View(rOLA);
        }

        // GET: Rola/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Rola/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_ROLA,OPIS,NAPOMENA")] ROLA rOLA)
        {
            if (ModelState.IsValid)
            {
                db.ROLA.Add(rOLA);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(rOLA);
        }

        // GET: Rola/Edit/5
        public ActionResult Edit(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ROLA rOLA = db.ROLA.Find(id);
            if (rOLA == null)
            {
                return HttpNotFound();
            }
            return View(rOLA);
        }

        // POST: Rola/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_ROLA,OPIS,NAPOMENA")] ROLA rOLA)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rOLA).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(rOLA);
        }

        // GET: Rola/Delete/5
        public ActionResult Delete(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ROLA rOLA = db.ROLA.Find(id);
            if (rOLA == null)
            {
                return HttpNotFound();
            }
            return View(rOLA);
        }

        // POST: Rola/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(decimal id)
        {
            ROLA rOLA = db.ROLA.Find(id);
            db.ROLA.Remove(rOLA);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

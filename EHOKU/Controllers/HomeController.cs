﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EHOKU.Models;
using System.Data.SqlClient;

namespace EHOKU.Controllers
{
    public class HomeController : Controller
    {
        private EHOKUEntities db = new EHOKUEntities();

        public ActionResult Index()
        {

            int v_Osoba = Convert.ToInt32(Osoba.Currentuser.ID);
            int v_OsobaRola = Convert.ToInt32(Osoba.Currentuser.ROLA);

            if (v_OsobaRola == 1 || v_OsobaRola == 2)
            {

                dashboard D = new dashboard();
                D.broj1 = db.POSAO.Count();
                D.broj2 = db.POSAO.Where(x => x.ID_STATUS_POSLA == 1).Count();
                D.broj3 = db.POSAO.Where(x => x.ID_STATUS_POSLA == 2).Count();
                D.broj4 = db.POSAO.Where(x => x.ID_STATUS_POSLA == 3).Count();

                D.lista = db.Database.SqlQuery<StatistikaRadnik>("SELECT ID_RADNIK, IME, PREZIME, OPIS, ZAVRSENIH_POSLOVA, ZARADENO, OCJENA FROM STATISTIKA_RADNIK");
                return View(D);

            }
            else if (v_OsobaRola == 3 || v_OsobaRola == 4)
            {

                dashboard D = new dashboard();
                D.broj1 = db.POSAO.Where(x => x.ID_RADNIK == v_Osoba || x.ID_RADNIK == null).Count();
                D.broj2 = db.POSAO.Where(x => x.ID_STATUS_POSLA == 1).Count();
                D.broj3 = db.POSAO.Where(x => x.ID_STATUS_POSLA == 2 && x.ID_RADNIK == v_Osoba).Count();
                D.broj4 = db.POSAO.Where(x => x.ID_STATUS_POSLA == 3 && x.ID_RADNIK == v_Osoba).Count();

                D.lista = db.Database.SqlQuery<StatistikaRadnik>("SELECT ID_RADNIK, IME, PREZIME, OPIS, ZAVRSENIH_POSLOVA, ZARADENO, OCJENA FROM STATISTIKA_RADNIK");
                return View(D);

            }

            else
            {
                return View();
            }
                    
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(OSOBA u)
        {
            //ova akcija se koristi za hendlanje post-a
            if(ModelState.IsValid) // ovo je za provjeru validnosti
            {
                using (EHOKUEntities dc = new EHOKUEntities())
                {
                    var v = dc.OSOBA.Where(a => a.IME.Equals(u.IME) && a.OIB.Equals(u.OIB)).FirstOrDefault();
                  
                    if (v != null)
                    {
                        //Session["LogedUserID"] = v.ID_OSOBA.ToString();
                        //Session["LogedUserFullName"] = v.IME + ' ' + v.PREZIME.ToString();
                        //Session["LogedUserRole"] = v.ID_ROLA.ToString();
                        //return RedirectToAction("AfterLogin");

                        Osoba o = new Osoba();
                        o.ID = v.ID_OSOBA.ToString();
                        o.IME = v.IME + ' ' + v.PREZIME.ToString();
                        o.ROLA = v.ID_ROLA.ToString();
                        Session["LogedUser"] = o;

                        System.Web.Security.FormsAuthentication.SetAuthCookie(o.IME, false);
                        return RedirectToAction("Index");

                    }

                }
            }
            return View(u);
        }
   

        public ActionResult AfterLogin()
        {
            if(Osoba.Currentuser != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        public ActionResult Logout()
        {
          //  Session.Clear();
            Session.Abandon();
            System.Web.Security.FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Home");
        }


        public JsonResult GetWeather()
        {
            Weather weath = new Weather();
            return Json(weath.getWeatherForcast(), JsonRequestBehavior.AllowGet);
        }
    }
}
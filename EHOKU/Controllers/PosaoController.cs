﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EHOKU.Models;


namespace EHOKU.Controllers
{
    public class PosaoController : Controller
    {
        private EHOKUEntities db = new EHOKUEntities();

        // GET: Posao
        public ActionResult Index()
        {
            //varijabla generirana
            ViewBag.ID_STATUS_POSLA = new SelectList(db.STATUS_POSLA, "ID_STATUS_POSLA", "OPIS");
            int v_Osoba = Convert.ToInt32(Osoba.Currentuser.ID);
            int v_OsobaRola = Convert.ToInt32(Osoba.Currentuser.ROLA);

            if (v_OsobaRola == 2)
            {
                var pOSAO = db.POSAO.Where(a => a.ID_VODITELJ == v_Osoba || a.ID_VODITELJ == null).Include(p => p.OSOBA).Include(p => p.OSOBA1).Include(p => p.OSOBA2).Include(p => p.STATUS_POSLA).Include(p => p.TIP_POSLA).OrderByDescending(p => p.DATUM_OBJAVE);

                return View(pOSAO.ToList());

            }
            else if(v_OsobaRola == 3 || v_OsobaRola == 4)
            {
                var pOSAO = db.POSAO.Where(a => a.ID_RADNIK == v_Osoba || a.ID_RADNIK == null).Include(p => p.OSOBA).Include(p => p.OSOBA1).Include(p => p.OSOBA2).Include(p => p.STATUS_POSLA).Include(p => p.TIP_POSLA).OrderByDescending(p => p.DATUM_OBJAVE);

                return View(pOSAO.ToList());

            }
            else
            {
                var pOSAO = db.POSAO.Include(p => p.OSOBA).Include(p => p.OSOBA1).Include(p => p.OSOBA2).Include(p => p.STATUS_POSLA).Include(p => p.TIP_POSLA).OrderByDescending(p => p.DATUM_OBJAVE);

                return View(pOSAO.ToList());
            }

        }

        //AKCIJA ZA FILTRIRANJE I PRETRAŽIVANJE
        [HttpPost]
        public ActionResult Index(int? ID_STATUS_POSLA, string searchString)
        {

            ViewBag.ID_STATUS_POSLA = new SelectList(db.STATUS_POSLA, "ID_STATUS_POSLA", "OPIS");
            int v_Osoba = Convert.ToInt32(Osoba.Currentuser.ID);
            int v_OsobaRola = Convert.ToInt32(Osoba.Currentuser.ROLA);
     
           
         
            if (v_OsobaRola == 2)
            {
                IEnumerable<POSAO> posao = db.POSAO.Where(a => a.ID_STATUS_POSLA == ID_STATUS_POSLA || ID_STATUS_POSLA == null).Where(a => a.ID_VODITELJ == v_Osoba || a.ID_VODITELJ == null).Where(x => x.OPIS_POSLA.Contains(searchString)).OrderByDescending(p => p.DATUM_OBJAVE);

                return View(posao);

            }
            else if (v_OsobaRola == 3 || v_OsobaRola == 4)
            {
                IEnumerable<POSAO> posao = db.POSAO.Where(a => a.ID_STATUS_POSLA == ID_STATUS_POSLA || ID_STATUS_POSLA == null).Where(a => a.ID_RADNIK == v_Osoba || a.ID_RADNIK == null).Where(x => x.OPIS_POSLA.Contains(searchString)).OrderByDescending(p => p.DATUM_OBJAVE);

                return View(posao);

            }
            else
            {
                IEnumerable<POSAO> posao = db.POSAO.Where(a => a.ID_STATUS_POSLA == ID_STATUS_POSLA || ID_STATUS_POSLA == null).Where(x => x.OPIS_POSLA.Contains(searchString)).OrderByDescending(p => p.DATUM_OBJAVE);

                return View(posao);
            }
        }



        // GET: Posao/Details/5
        public ActionResult Details(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            POSAO pOSAO = db.POSAO.Find(id);
            if (pOSAO == null)
            {
                return HttpNotFound();
            }
            return View(pOSAO);
        }

        // GET: Posao/Create
        public ActionResult Create()
        {
            var v_osoba = db.OSOBA.Where(g => g.ID_ROLA == 5).OrderBy(g => g.IME).ToList().Select(x => new SelectListItem { Value = x.ID_OSOBA.ToString(), Text = x.IME + " " + x.PREZIME});
            //IEnumerable<SelectListItem> selectList = from g in v_osoba
            //                                         select new SelectListItem
            //                                         {
            //                                             Value = g.ID_OSOBA.ToString(),
            //                                             Text = g.IME + " " + g.PREZIME.ToString()
            //                                         };
            ViewBag.ID_KLIJENT = new SelectList(v_osoba, "Value", "Text");

            var v_osoba1 = db.OSOBA.Where(g => g.ID_ROLA == 3 || g.ID_ROLA == 4).OrderBy(g => g.IME).ToList();
            IEnumerable<SelectListItem> selectList1 = from g in v_osoba1
                                                     select new SelectListItem
                                                     {
                                                         Value = g.ID_OSOBA.ToString(),
                                                         Text = g.IME + " " + g.PREZIME.ToString()
                                                     };
            ViewBag.ID_RADNIK = new SelectList(selectList1, "Value", "Text");

            var v_osoba2 = db.OSOBA.Where(g => g.ID_ROLA == 2).OrderBy(g => g.IME).ToList();
            IEnumerable<SelectListItem> selectList2 = from g in v_osoba2
                                                      select new SelectListItem
                                                      {
                                                          Value = g.ID_OSOBA.ToString(),
                                                          Text = g.IME + " " + g.PREZIME.ToString()
                                                      };
            ViewBag.ID_VODITELJ = new SelectList(selectList2, "Value", "Text");

        
            //ViewBag.ID_KLIJENT = new SelectList(db.OSOBA.Where(g => g.ID_ROLA == 5), "ID_OSOBA", "IME");
            //ViewBag.ID_RADNIK = new SelectList(db.OSOBA.Where(g => g.ID_ROLA == 3), "ID_OSOBA", "IME");
            //ViewBag.ID_VODITELJ = new SelectList(db.OSOBA.Where(g => g.ID_ROLA == 1), "ID_OSOBA", "IME");
            ViewBag.ID_STATUS_POSLA = new SelectList(db.STATUS_POSLA, "ID_STATUS_POSLA", "OPIS");
            ViewBag.ID_TIP_POSLA = new SelectList(db.TIP_POSLA, "ID_TIP_POSLA", "OPIS");
            return View();
        }



        // POST: Posao/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_POSAO,ID_STATUS_POSLA,ID_KLIJENT,ID_VODITELJ,ID_RADNIK,VRIJEME_POCETKA,VRIJEME_ZAVRSETKA,ROK_IZVEDBE,IZNOS,NAPLACENO,ISPLACENO,OCJENA_OBJAVLJENOG_POSLA,NAPOMENA,OPIS_POSLA,PRILOG,ID_TIP_POSLA")] POSAO pOSAO)
        {

                int v_Osoba = Convert.ToInt32(Osoba.Currentuser.ROLA);
                decimal v1_Osoba = Convert.ToDecimal(Osoba.Currentuser.ID);


            if (ModelState.IsValid)
                {
                    pOSAO.ID_VODITELJ = v1_Osoba;
                    db.POSAO.Add(pOSAO);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
           
            ViewBag.ID_KLIJENT = new SelectList(db.OSOBA, "ID_OSOBA", "IME", pOSAO.ID_KLIJENT);
            ViewBag.ID_RADNIK = new SelectList(db.OSOBA, "ID_OSOBA", "IME", pOSAO.ID_RADNIK);
            ViewBag.ID_VODITELJ = new SelectList(db.OSOBA, "ID_OSOBA", "IME", pOSAO.ID_VODITELJ);
            ViewBag.ID_STATUS_POSLA = new SelectList(db.STATUS_POSLA, "ID_STATUS_POSLA", "OPIS", pOSAO.ID_STATUS_POSLA);
            ViewBag.ID_TIP_POSLA = new SelectList(db.TIP_POSLA, "ID_TIP_POSLA", "OPIS", pOSAO.ID_TIP_POSLA);
            return View(pOSAO);
        }

        // GET: Posao/Edit/5
        public ActionResult Edit(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            POSAO pOSAO = db.POSAO.Find(id);
            if (pOSAO == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_KLIJENT = new SelectList(db.OSOBA, "ID_OSOBA", "IME", pOSAO.ID_KLIJENT);
            ViewBag.ID_RADNIK = new SelectList(db.OSOBA, "ID_OSOBA", "IME", pOSAO.ID_RADNIK);
            ViewBag.ID_VODITELJ = new SelectList(db.OSOBA, "ID_OSOBA", "IME", pOSAO.ID_VODITELJ);
            ViewBag.ID_STATUS_POSLA = new SelectList(db.STATUS_POSLA, "ID_STATUS_POSLA", "OPIS", pOSAO.ID_STATUS_POSLA);
            ViewBag.ID_TIP_POSLA = new SelectList(db.TIP_POSLA, "ID_TIP_POSLA", "OPIS", pOSAO.ID_TIP_POSLA);
            return View(pOSAO);
        }

        // POST: Posao/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_POSAO,ID_STATUS_POSLA,ID_KLIJENT,ID_VODITELJ,ID_RADNIK,VRIJEME_POCETKA,VRIJEME_ZAVRSETKA,ROK_IZVEDBE,IZNOS,NAPLACENO,ISPLACENO,OCJENA_OBJAVLJENOG_POSLA,NAPOMENA,OPIS_POSLA,PRILOG,ID_TIP_POSLA")] POSAO pOSAO)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pOSAO).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ID_KLIJENT = new SelectList(db.OSOBA, "ID_OSOBA", "IME", pOSAO.ID_KLIJENT);
            ViewBag.ID_RADNIK = new SelectList(db.OSOBA, "ID_OSOBA", "IME", pOSAO.ID_RADNIK);
            ViewBag.ID_VODITELJ = new SelectList(db.OSOBA, "ID_OSOBA", "IME", pOSAO.ID_VODITELJ);
            ViewBag.ID_STATUS_POSLA = new SelectList(db.STATUS_POSLA, "ID_STATUS_POSLA", "OPIS", pOSAO.ID_STATUS_POSLA);
            ViewBag.ID_TIP_POSLA = new SelectList(db.TIP_POSLA, "ID_TIP_POSLA", "OPIS", pOSAO.ID_TIP_POSLA);
            return View(pOSAO);
        }

        // GET: Posao/Delete/5
        public ActionResult Delete(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            POSAO pOSAO = db.POSAO.Find(id);
            if (pOSAO == null)
            {
                return HttpNotFound();
            }
            return View(pOSAO);
        }

        // POST: Posao/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(decimal id)
        {
            POSAO pOSAO = db.POSAO.Find(id);
            db.POSAO.Remove(pOSAO);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        //METODA PRIHVATI POSAO
        [HttpGet]
        public ActionResult PrihvatioPosao(int  id)
        {
            

            using (EHOKUEntities dc = new EHOKUEntities())
            {
               var posao =  dc.POSAO.SingleOrDefault( x => x.ID_POSAO == id);

                if (posao == null) return null;
                int v_Osoba = Convert.ToInt32(Osoba.Currentuser.ID);
                posao.ID_RADNIK = v_Osoba;
                posao.ID_STATUS_POSLA = 2;
                dc.SaveChanges();
            }
            return RedirectToAction("Index");

           
        }



        //METODA ZAVRSI POSAO
        // GET: Posao/ZavrsiPosao/5
        public ActionResult ZavrsiPosao(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            POSAO pOSAO = db.POSAO.Find(id);
            if (pOSAO == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_KLIJENT = new SelectList(db.OSOBA, "ID_OSOBA", "IME", pOSAO.ID_KLIJENT);
            ViewBag.ID_RADNIK = new SelectList(db.OSOBA, "ID_OSOBA", "IME", pOSAO.ID_RADNIK);
            ViewBag.ID_VODITELJ = new SelectList(db.OSOBA, "ID_OSOBA", "IME", pOSAO.ID_VODITELJ);
            ViewBag.ID_STATUS_POSLA = new SelectList(db.STATUS_POSLA, "ID_STATUS_POSLA", "OPIS", pOSAO.ID_STATUS_POSLA);
            ViewBag.ID_TIP_POSLA = new SelectList(db.TIP_POSLA, "ID_TIP_POSLA", "OPIS", pOSAO.ID_TIP_POSLA);
            return View(pOSAO);
        }

        // POST: Posao/ZavrsiPosao/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ZavrsiPosao([Bind(Include = "ID_POSAO,ID_STATUS_POSLA,VRIJEME_POCETKA,VRIJEME_ZAVRSETKA,NAPOMENA")] POSAO pOSAO)
        {
            if (ModelState.IsValid)
            {
                var v_posao = db.POSAO.FirstOrDefault(p => p.ID_POSAO == pOSAO.ID_POSAO);
                if (v_posao == null)
                    return HttpNotFound();
                
                if(v_posao.ID_STATUS_POSLA == 2)
                {
                    v_posao.ID_STATUS_POSLA = 3;
                    v_posao.VRIJEME_POCETKA = pOSAO.VRIJEME_POCETKA;
                    v_posao.VRIJEME_ZAVRSETKA = pOSAO.VRIJEME_ZAVRSETKA;
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            ViewBag.ID_KLIJENT = new SelectList(db.OSOBA, "ID_OSOBA", "IME", pOSAO.ID_KLIJENT);
            ViewBag.ID_RADNIK = new SelectList(db.OSOBA, "ID_OSOBA", "IME", pOSAO.ID_RADNIK);
            ViewBag.ID_VODITELJ = new SelectList(db.OSOBA, "ID_OSOBA", "IME", pOSAO.ID_VODITELJ);
            ViewBag.ID_STATUS_POSLA = new SelectList(db.STATUS_POSLA, "ID_STATUS_POSLA", "OPIS", pOSAO.ID_STATUS_POSLA);
            ViewBag.ID_TIP_POSLA = new SelectList(db.TIP_POSLA, "ID_TIP_POSLA", "OPIS", pOSAO.ID_TIP_POSLA);
            return View(pOSAO);
        }


        //METODA ZA OCJENU POSLA
        // GET: Posao/OcijeniPosao/5
        public ActionResult OcijeniPosao(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            POSAO pOSAO = db.POSAO.Find(id);
            if (pOSAO == null)
            {
                return HttpNotFound();
            }
            
            return View(pOSAO);
        }

        // POST: Posao/OcijeniPosao/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult OcijeniPosao([Bind(Include = "ID_POSAO,OCJENA_OBJAVLJENOG_POSLA")] POSAO pOSAO)
        {
            if (ModelState.IsValid)
            {
                var v_posao = db.POSAO.FirstOrDefault(p => p.ID_POSAO == pOSAO.ID_POSAO);
                if (v_posao == null)
                return HttpNotFound();

                if(v_posao.ID_STATUS_POSLA == 3 && v_posao.OCJENA_OBJAVLJENOG_POSLA == null)
                {
                    v_posao.OCJENA_OBJAVLJENOG_POSLA = pOSAO.OCJENA_OBJAVLJENOG_POSLA;
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }   
            return View(pOSAO);
        }

    }
}




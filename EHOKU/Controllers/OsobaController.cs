﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace EHOKU.Controllers
{
    public class OsobaController : Controller
    {
        private EHOKUEntities db = new EHOKUEntities();

        // GET: Osoba
        public ActionResult Index()
        {
            var oSOBA = db.OSOBA.Include(o => o.ROLA);
            return View(oSOBA.ToList());
        }

        // GET: Osoba/Details/5
        public ActionResult Details(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OSOBA oSOBA = db.OSOBA.Find(id);
            if (oSOBA == null)
            {
                return HttpNotFound();
            }
            return View(oSOBA);
        }

        // GET: Osoba/Create
        public ActionResult Create()
        {
            ViewBag.ID_ROLA = new SelectList(db.ROLA, "ID_ROLA", "OPIS");
            return View();
        }

        // POST: Osoba/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_OSOBA,OIB,IME,PREZIME,SPOL,KONTAKT_BROJ,EMAIL,ID_ROLA,ULICA,NASELJE,MJESTO")] OSOBA oSOBA)
        {
            if (ModelState.IsValid)
            {
                db.OSOBA.Add(oSOBA);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ID_ROLA = new SelectList(db.ROLA, "ID_ROLA", "OPIS", oSOBA.ID_ROLA);
            return View(oSOBA);
        }

        // GET: Osoba/Edit/5
        public ActionResult Edit(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OSOBA oSOBA = db.OSOBA.Find(id);
            if (oSOBA == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_ROLA = new SelectList(db.ROLA, "ID_ROLA", "OPIS", oSOBA.ID_ROLA);
            return View(oSOBA);
        }

        // POST: Osoba/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_OSOBA,OIB,IME,PREZIME,SPOL,KONTAKT_BROJ,EMAIL,ID_ROLA,ULICA,NASELJE,MJESTO")] OSOBA oSOBA)
        {
            if (ModelState.IsValid)
            {
                db.Entry(oSOBA).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ID_ROLA = new SelectList(db.ROLA, "ID_ROLA", "OPIS", oSOBA.ID_ROLA);
            return View(oSOBA);
        }

        // GET: Osoba/Delete/5
        public ActionResult Delete(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OSOBA oSOBA = db.OSOBA.Find(id);
            if (oSOBA == null)
            {
                return HttpNotFound();
            }
            return View(oSOBA);
        }

        // POST: Osoba/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(decimal id)
        {
            OSOBA oSOBA = db.OSOBA.Find(id);
            db.OSOBA.Remove(oSOBA);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
